# Privacy-enhancing technologies Hands-on Workshop API

This project implements the API of the [PET hands-on workshop](https://gitlab.inria.fr/spicy/pow)

## Install

In order to install it on a virtual environment, execute the commands below:

```bash
# clone the project and enter to the repo dir
git clone https://gitlab.inria.fr/spicy/pow-api.git pow-api && cd $_
# create a virtual environment
python3.8 -m venv ${PATH}/venv-pow-api
# activate the environment
source ${PATH}/venv-pow-api/bin/activate
# install the dependency management tool
pip install poetry
# install dependencies on the environment
poetry install
```

## Code organization

The API is organized in several modules contained in `atelier`:

- `data` includes methods to process a Panda's data frame such as
  _merge_, _resample_, _aggregate_, etc.
- `io` includes the methods to read and write data from/to the file
  filesystem. Currently the API supports the _parquet_ format as
  serialization method.
- `utils` includes utilitary methods and classes required in other
  modules such as calendar-related operations.
- `plot` includes methods organized by scope (associated to other
  sub-modules) such as `timeline` plots for sequential data; plots of
  anonymity `metrics`; summary plots of a model registered during the
  `learn` process; plots to show a general overview of the datasets
  resulting from `privacy` method executions; and plots to show
  `geo`-spatial data.
  data
- `privacy` includes methods to protect data such as perturbation
  mechanisms of _Laplace_ and an implementation of _FPA_ in the `rastogi`
  sub-module.
- `metrics` includes privacy-enhancing metrics implementation of
  _anonymity sets_ and _Shannon entropy_.
- `learn` includes implementations of machine learning algorithms based
  on the logic implemented by _scikit-learn_ (i.e., transformers,
  predictors, pipelines, etc.): `model` implements a common
  implementation of a predictor _pipeline_, and a default regressor
  model; `preprocessing` implements _function transformers_, a feature
  extraction method for sequential data, and dataset splitter of train,
  validate and test subsets.
- `extensions` includes methods specific for a use-case that requires
  additional implementations that are not offered in other sub-modules.


## Contribute

To add new features to the workshop API, please create a merge request
with an example. Bugs reports or comments are also welcome.
