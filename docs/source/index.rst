.. pow-api documentation master file, created by
   sphinx-quickstart on Thu Oct 28 17:31:06 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

`pow-api` API documentation!
============================

.. toctree::
   :maxdepth: 1
   :caption: API:
   
   autoapi/atelier/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


