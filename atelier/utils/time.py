from typing import Union

from pandas import DataFrame, DatetimeIndex, Timedelta, Timestamp
from pandas.tseries.holiday import AbstractHolidayCalendar, Holiday


def get_next_monday(
    dataframe: DataFrame,
    weeks: int,
) -> Timestamp:
    """Get next monday after an offset in weeks.

    The dataset is only required to get the minimum value from its
    `DatetimeIndex`. Then the next Monday is calculated for an offset
    in number of weeks.

    :param dataframe: the reference dataset with a datetime index
    :type dataframe: DataFrame
    :param weeks: number of weeks to use as offset
    :type weeks: int
    :return: `Timestamp` representing the next Monday after the offset.
    :rtype: Timestamp
    """
    timedelta = Timedelta(7 * weeks - 1, unit="day")
    timestamp = dataframe.index.min() + timedelta
    return timestamp.normalize()


class HolidayCalendar(AbstractHolidayCalendar):
    """Generic implementation of a holyday calendar."""

    def __init__(
        self,
        timeframes: Union[Timestamp, DatetimeIndex],
    ):
        """Initialize the calendar by setting the holidays provided as
        `Timestamp` or date ranges.

        :param timeframes: datetime-kind value used to create the cal
        :type timeframes: Union[Timestamp, DatetimeIndex]
        """
        super().__init__()
        for holiday in timeframes:
            if isinstance(holiday, Timestamp):
                self.rules.append(
                    Holiday(
                        "",
                        holiday.year,
                        holiday.month,
                        holiday.day,
                    )
                )
            else:
                holidays = [Holiday("", t.year, t.month, t.day) for t in holiday]
                self.rules.append(holidays)
