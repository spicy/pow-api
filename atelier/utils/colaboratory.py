import importlib
from importlib import util

import plotly.io as pio
from IPython import get_ipython

GOOGLE_COLAB = "google.colab"


def setup() -> bool:
    """Utility to check if the running kernel is `colab`.

    When `colab` is detected as running kernel the method also loads
    the "google.colab" module, and enable its dataframe formatter.
    Finally the default renderer of `plotly` is set "colab".

    :return: `True` when the running example is `colab`
    :rtype: bool
    """
    instance = get_ipython()
    registered = True if GOOGLE_COLAB in str(instance) else False
    if registered:
        pio.renderers.default = "colab"
        spec = util.find_spec(GOOGLE_COLAB)
        if spec:
            module = ".".join([GOOGLE_COLAB, "data_table"])
            data_table = importlib.import_module(module)
            enable_dataframe_formatter = getattr(
                data_table,
                "enable_dataframe_formatter",
            )

            enable_dataframe_formatter()

    return registered
