from numpy import ndarray, random


def lpa(Q: ndarray, δ: float, ε: float, random_state: int = None) -> ndarray:
    """Add noise to a sequence with a Laplacian mechanism.

    :param Q: Sequence (1-D) to perturbe
    :type Q: ndarray
    :param δ: sensitivity
    :type δ: float
    :param ε: differential privacy budget
    :type ε: float
    :param random_state: random seed, defaults to None
    :type random_state: int, optional
    :return: A sequence with laplacian noise.
    :rtype: ndarray
    """

    # random generator with seed
    generator = random.default_rng(seed=random_state)

    # differential privacy scale based on the budget
    λ = δ / ε

    # Laplace mechanism applied to whole serie
    Z = generator.laplace(scale=λ, size=Q.size)

    return Q + Z
