import math
from typing import Optional

import numpy as np
from numpy import linalg, ndarray
from pandas import NA, DataFrame, Series

from . import mechanism


def fpa(Q: ndarray, δ: float, ε: float, k: int, random_state: int = None) -> ndarray:
    """Add noise to a sequence applying the FPA mechanism.

    :param Q: Sequence (1-D array) to perturb
    :type Q: ndarray
    :param δ: sensitivity
    :type δ: float
    :param ε: differential privacy budget
    :type ε: float
    :param k: Fourier coefficients
    :type k: int
    :param random_state: random seed, defaults to None
    :type random_state: int, optional
    :return: A sequence with FPA noise
    :rtype: ndarray
    """

    # discrete Fourier trasform
    F = np.fft.fft(Q)

    # first k values of DFT
    F_k = F[:k]

    # lpa of F_k
    Fλ_k = mechanism.lpa(F_k.real, δ, ε, random_state) + (
        1j * mechanism.lpa(F_k.imag, δ, ε, random_state)
    )

    # Fλ_k with `n - k` zero-padding
    Fλ_n = np.pad(Fλ_k, (0, Q.size - k))

    # inverse discrete Fourier transform
    Qλ = np.fft.ifft(Fλ_n)

    # modulus of complex values of IFFT
    Qλ_m = np.absolute(Qλ)

    # round perturbation to integers
    Qλ_int = np.rint(Qλ_m)

    # replace negative values with zeroes
    Qλ_int[Qλ_int < 0] = 0

    return Qλ_int


def fourier_perturbation(
    sequence: Series,
    boundary: float,
    budget: float,
    coefficients: int,
    *,
    random_state: Optional[int] = None,
) -> Optional[ndarray]:
    """Add noise to a time serie applying the FPA mechanism of Rastogi.

    :param sequence: time series to perturb
    :type sequence: Series
    :param boundary: noise boundary associated to the series
    :type boundary: float
    :param budget: differential privacy budget
    :type budget: float
    :param coefficients: number of Fourier coefficients
    :type coefficients: int
    :param random_state: random seed, defaults to None
    :type random_state: Optional[int], optional
    :return: A time series with FPA noise
    :rtype: Optional[ndarray]
    """

    def norm(seed: float, size: int, order: int) -> float:
        """Calculate the L-norm of a uniform vector of seed values."""
        serie = np.full((size,), seed)
        return linalg.norm(serie, order)

    size = sequence.size
    if size > coefficients:
        sensitivity = math.sqrt(coefficients) * norm(boundary, size, 2)
        return fpa(
            sequence.to_numpy(),
            sensitivity,
            budget,
            coefficients,
            random_state=random_state,
        )

    return None


def fourier_perturbation_by_timeframe(
    sequence: Series,
    boundary: float,
    epsilon: float,
    coefficients: int,
    *,
    period: str = "week",
    random_state: Optional[int] = None,
) -> ndarray:
    """Add noise to a time serie applying the FPA by a given time frame.

    :param sequence: time series to perturb
    :type sequence: Series
    :param boundary: noise boundary associated to the series
    :type boundary: float
    :param budget: differential privacy budget
    :type budget: float
    :param coefficients: number of Fourier coefficients
    :type coefficients: int
    :param period: [description], defaults to "week"
    :type period: str, optional
    :param random_state: random seed, defaults to None
    :type random_state: Optional[int], optional
    :return: A time series with FPA noise
    :rtype: ndarray
    """

    def get_period(dataframe: DataFrame, period: str) -> Optional[Series]:
        return {
            "day": dataframe.index.date,
            "week": dataframe.index.isocalendar().week,
            "month": dataframe.index.month_name(),
            "year": dataframe.index.year,
        }.get(period)

    dataframe = sequence.copy().to_frame()
    dataframe["period"] = get_period(dataframe, period)
    fpas = []
    for period in dataframe["period"].unique():
        period_sequence = dataframe[dataframe["period"] == period]
        period_fpa = fourier_perturbation(
            period_sequence.iloc[:, 0],
            boundary,
            epsilon,
            coefficients,
            random_state=random_state,
        )

        fpas.append(period_fpa)

    dataframe["fpa"] = np.concatenate(fpas).ravel()
    return dataframe["fpa"].to_numpy()


def bound(
    serie: Series,
    aggregate: str,
) -> float:
    """Get the bound of a given time series for an aggregate function.

    The bounds of a time series are arbitrarily defined for the `count`
    aggregate to `1`, and for the `sum` aggregate to the `math.ceil` of
    the maximum value of the serie. These values do not correspond to
    any formalization and they are only valid based on heuristics since
    the bound is intrinsically associated to the nature of the data.

    :param serie: a time serie
    :type serie: Series
    :param aggregate: name of the aggregate
    :type aggregate: str
    :return: Bound of the series according to the aggregate
    :rtype: float
    """

    def ceil(serie: Series) -> float:
        maximum = serie.max()
        # maximum = linalg.norm(Q, np.inf)
        # # round(maximum, -1)
        return 10 * math.ceil(maximum / 10)

    return {
        "count": 1,
        "sum": ceil(serie),
    }.get(aggregate, NA)
