from typing import Sequence, Tuple

from folium import CircleMarker, Map
from folium.plugins import HeatMapWithTime
from IPython import display
from pandas import DataFrame

# TILES = "https://{s}.basemaps.cartocdn.com/dark_nolabels/{z}/{x}/{y}{r}.png",
TILES = "https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png"
TILES_ATTRIBUTION = "CartoDB"

# show dataset on a map
def heatmap_plot(
    dataframe: DataFrame,
    *,
    group_column: str = "departure_time",
    gps_columns: Tuple[str, str] = ("stop_lat", "stop_lon"),
    # initial map zoom
    zoom: int = 11,
    # Rennes GPS coordinates
    location: Tuple[float, float] = (48.1147, -1.6794),
) -> None:
    _dataframe = dataframe.copy(deep=True)
    timestamps = []
    coordinates = []
    for timestamp, coordinate in _dataframe.groupby(group_column):
        timestamps.append(str(timestamp))
        coordinates.append(
            coordinate[
                [
                    gps_columns[0],
                    gps_columns[1],
                ]
            ].values.tolist()
        )

    base_map = Map(
        location=location,
        zoom_start=zoom,
        tiles=TILES,
        attr=TILES_ATTRIBUTION,
    )

    heat_map = HeatMapWithTime(
        data=coordinates,
        index=timestamps,
        auto_play=True,
        min_speed=1,
        radius=4,
        max_opacity=0.5,
    )

    heat_map.add_to(base_map)
    display.display(base_map)


def saliency_plot(
    # lat, lon, weight, label
    dataset: Sequence,
    *,
    zoom: int = 11,
    location: Tuple[float, float] = (48.1147, -1.6794),
    partitions: Tuple[int, int, int] = (5000, 6250, 7500),
    min_threshold: int = 100,
):
    base_map = Map(
        location=location,
        zoom_start=zoom,
        tiles=TILES,
        attr=TILES_ATTRIBUTION,
    )

    for item in dataset:
        popup = f"<b>{item[3]}</b><br><b>{item[2]}</b>"
        radius = 2
        weight = item[2]
        if weight > partitions[2]:
            color = "#581845"
        elif partitions[1] < weight <= partitions[2]:
            color = "#c70039"
        elif partitions[0] < weight <= partitions[1]:
            color = "#ff5733"
        elif min_threshold < weight <= partitions[0]:
            color = "#f3b54a"
        else:
            color = "#839B5c"
            radius = 1

        CircleMarker(
            (item[0], item[1]),
            popup=popup,
            fill=True,
            radius=radius,
            color=color,
            fill_color=color,
        ).add_to(base_map)

    display.display(base_map)
