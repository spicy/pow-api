from typing import Callable, Dict, List, Optional, Tuple, Union

import pandas as pd
from pandas import DataFrame, Timedelta


def merge_dataframes(
    left: DataFrame,
    right: DataFrame,
    *,
    how: str = "outer",
    fillna: bool = True,
    fillna_value: Union[str, float, int, bool] = 0,
) -> DataFrame:
    """Merge datasets which index is of type `DatetimeIndex`.

    :param left: first or left dataset to merge
    :type left: DataFrame
    :param right: second or right dataset to merge
    :type right: DataFrame
    :param how: type of merge, defaults to "outer"
    :type how: str, optional
    :param fillna: fill NaN values after merge , defaults to True
    :type fillna: bool, optional
    :param fillna_value: default fill value, defaults to 0
    :type fillna_value: Union[str, float, int, bool], optional
    :return: A merged dataset
    :rtype: DataFrame
    """
    left_max = left.index.max()
    right_max = right.index.max()
    farest = right_max if right_max <= left_max else left_max
    farest += Timedelta(1, unit="day")
    left_ = left[left.index <= farest]
    right_ = right[right.index <= farest]

    # merge datasets
    dataframe = pd.merge(
        left_,
        right_,
        how=how,
        left_index=True,
        right_index=True,
    )

    # fill empty values with zeroes
    if fillna:
        dataframe = dataframe.fillna(fillna_value)

    return dataframe


def split_dataframe(
    dataframe: DataFrame,
    target: str,
    reset_index: bool = False,
) -> Tuple[DataFrame, DataFrame]:
    """Split a dataframe in inputs and outputs: (X, y).

    `scikit-learn` uses as convention two sets `X` and `y` of data to
    perform training and transformation operations. Given a dataset
    with multiple attributes (columns), the split create two datasets,
    the first with all the attributes but the `target` used as `X` and
    the `target` attribute used as `y`.

    :param dataframe: dataset to split
    :type dataframe: DataFrame
    :param target: target attribute (column) to use as `y`
    :type target: str
    :param reset_index: reset `X` index after split, defaults to False
    :type reset_index: bool, optional
    :return: Two datasets representing `X` and `y`
    :rtype: Tuple[DataFrame, DataFrame]
    """
    y = dataframe[[target]]
    X = dataframe.drop(columns=[target])
    if reset_index:
        X = X.reset_index()

    return X, y


def aggregate_dataframe(
    dataframe: DataFrame,
    *,
    by: Union[str, List[str]],
    agg: Union[str, Callable, Dict[str, Union[str, Callable]]],
    attribute: Optional[str] = None,
    value: Optional[Union[str, List[str]]] = None,
    keep_index: bool = True,
) -> DataFrame:
    """Dataset aggregation wrapper.

    This is a wrapper of the `groupby` method provided in Pandas.  The
    dataset must be sequential data (with index of type `DatetimeIndex`)
    When the optional `attribute` is set, select only rows a `value`
    equals to that attribute.

    :param dataframe: dataset to aggregate
    :type dataframe: DataFrame
    :param by: attributes (columns) to group by
    :type by: Union[str, List[str]]
    :param agg: aggregation function or name used to aggregate
    :type agg: Union[str, Callable, Dict[str, Union[str, Callable]]]
    :param attribute: name of an optional attribute used to filter among
        the dataset entries (rows), defaults to None
    :type attribute: Optional[str], optional
    :param value: value associated to the `attribute` to use during
        filtering, defaults to None
    :type value: Optional[Union[str, List[str]]], optional
    :param keep_index: keep all index of the original dataset even the
        result does not contain aggregated data, defaults to True
    :type keep_index: bool, optional
    :return: An aggregate dataset
    :rtype: DataFrame
    """
    dataframe_ = dataframe.copy()
    if attribute and value:
        values = [value] if isinstance(value, str) else value
        dataframe_ = dataframe_[dataframe_[attribute].isin(values)]

    if attribute and keep_index:
        by_ = [by] if isinstance(by, str) else by
        by_.append(attribute)

        # first groupby to keep all times of 'attribute'
        dataframe_ = dataframe_.groupby(by_).agg(agg).reset_index()

    return dataframe_.groupby(by).aggregate(agg)


def shift_datetime_index(
    dataframe: DataFrame,
    timedelta: Timedelta,
    *,
    attribute: Optional[str] = None,
    value: Optional[Union[str, List[str]]] = None,
) -> DataFrame:
    """Shift the index (type DatetimeIndex) by an offset.

    The dataset must be sequential data. When the optional `attribute`
    is set, shit only the index associated with the `value`.

    :param dataframe: dataset to change index totally or partially
    :type dataframe: DataFrame
    :param timedelta: offset used to shift the index
    :type timedelta: Timedelta
    :param attribute: name of an optional attribute used to filter among
        the dataset entries (rows), defaults to None
    :type attribute: Optional[str], optional
    :param value: value associated to the `attribute` to use during
        filtering, defaults to None
    :type value: Optional[Union[str, List[str]]], optional
    :return: A dataset with it index (partially) shifted by an offset
    :rtype: DataFrame
    """
    dataframe_ = dataframe.copy()
    dataframe_.reset_index(inplace=True)
    index = dataframe_.columns[0]
    if attribute and value:
        values = [value] if isinstance(value, str) else value
        for value in values:
            labels = dataframe_[attribute] == value
            dataframe_.loc[labels, index] += timedelta
    else:
        dataframe_.loc[:, index] += timedelta

    dataframe_.set_index(index, inplace=True)
    return dataframe_


def resample_dataframe(
    dataframe: DataFrame,
    frequency: Union[Timedelta, str],
) -> DataFrame:
    """Resample a dataset using the smallest index value as `start`.

    This is a wrapper of the resample method provided in Pandas fixing
    the `origin` parameter to `start` so the resulting dataset begins
    with the same value of the original dataset. This operation can be
    interpreted as an aggretion by frequency when it is a DateTimeIndex.

    :param dataframe: dataset to resample
    :type dataframe: DataFrame
    :param frequency: offset to use for resampling
    :type frequency: Union[Timedelta, str]
    :return: A resampled dataset
    :rtype: DataFrame
    """
    dataframe_ = dataframe.copy()

    # from pandas.tseries import frequencies
    # frequency = frequencies.to_offset(frequency)

    # dataframe_ = dataframe_.groupby(
    #     [
    #         pd.Grouper(
    #             freq=frequency,
    #             level=dataframe_.index.name,
    #             origin="start",
    #         )
    #     ]
    # ).sum()

    return dataframe_.resample(frequency, origin="start").sum()
