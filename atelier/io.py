import os
from errno import ENOENT
from pathlib import Path

import pyarrow.parquet as pq
from pandas import DataFrame


def read_data(
    path: Path,
) -> DataFrame:
    """Load dataset from file system.

    :param path: location path on the file system.
    :type path: Path
    :raises FileNotFoundError: When path is not available.
    :return: the dataset stored in the `path`
    :rtype: DataFrame
    """
    if not path.exists():
        raise FileNotFoundError(
            ENOENT,
            os.strerror(ENOENT),
            path,
        )

    table = pq.read_table(path)
    return table.to_pandas()
