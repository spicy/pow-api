import copy
from typing import List, Optional

from pandas import DataFrame, Series


def anoymity_sets(
    dataframe: DataFrame,
    subset: Optional[List[str]] = None,
    reindex: bool = False,
) -> DataFrame:
    """Get the annonymity sets of a dataset.

    :param dataframe: a dataset
    :type dataframe: DataFrame
    :param subset: subset of attributes (columns) to use while
        calculating the anonymity sets, when None is defined use
        all attributes of the dataset, defaults to None
    :type subset: Optional[List[str]], optional
    :param reindex: reindex resulting anonymity sets by filling non
        available values in a continuous axis with null occurences
        (as resampling with zeroes between two not consecutive anonymity
        sets), defaults to False
    :type reindex: bool, optional
    :return: The anonymity sets of a dataset
    :rtype: DataFrame
    """

    def reset_index(serie: Series) -> Series:
        """Reset index by including zero values"""
        domain = range(1, serie.index.max() + 1)
        return serie.reindex(domain, fill_value=0)

    multiplicity = dataframe.value_counts(subset=subset)
    _anonimity_sets = multiplicity.value_counts().sort_index()
    if reindex:
        _anonimity_sets = reset_index(_anonimity_sets)

    return _anonimity_sets


def get_anonymity_sets(
    dataframe: DataFrame,
    distinct: Optional[str] = None,
    *,
    subset: Optional[List[str]] = None,
    reindex: bool = False,
) -> Series:
    """Get the anonymity sets of a 'formatted' dataset.

    A 'formatted' dataset is defined as a dataset in which the `subset`
    does not contains duplicate values.

    :param dataframe: a dataset
    :type dataframe: DataFrame
    :param distinct: attribute to , defaults to None
    :type distinct: Optional[str], optional
    :param subset: subset of attributes (columns) to use while
        calculating the anonymity sets, when None is defined use
        all attributes of the dataset, defaults to None
    :type subset: Optional[List[str]], optional
    :param reindex: reindex resulting anonymity sets by filling non
        available values in a continuous axis with null occurences
        (as resampling with zeroes between two not consecutive anonymity
        sets), defaults to False
    :type reindex: bool, optional
    :return: The anonymity sets of a dataset
    :rtype: Series
    """

    def get_distinct(
        dataframe: DataFrame,
        subset: Optional[List[str]] = None,
        distinct: Optional[str] = None,
    ) -> DataFrame:
        """ "Select distinct columns by a defined attribute"""
        dataframe_ = dataframe.copy()
        if distinct:
            subset_ = copy.deepcopy(subset)
            if subset_:
                if distinct not in subset_:
                    subset_.append(distinct)
            else:
                subset_ = [distinct]

            dataframe_.drop_duplicates(subset=subset_, inplace=True)

        return dataframe_

    subset_ = None if not subset else subset
    dataframe_ = get_distinct(dataframe, subset_, distinct)
    _anonymity_sets = anoymity_sets(dataframe_, subset_, reindex)
    return (
        _anonymity_sets.to_frame()
        .reset_index()
        .rename(
            {
                "index": "cardinality",
                0: "occurrences",
            },
            axis=1,
        )
    )
