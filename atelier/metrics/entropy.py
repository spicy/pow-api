import numpy as np
from pandas import DataFrame, Series


# compute the entropy of a serie
def entropy(
    series: Series,
    base: int = 2,
    normalize: bool = False,
) -> float:
    """Compute the entropy of a series

    By default it computes the Shannon entropy (base two).

    :param series: a sequence
    :type series: Series
    :param base: log base of the entropy, defaults to 2
    :type base: int, optional
    :param normalize: when True the entropy is nomalized by computing
        the efficiency of the expectation fo the sequence,
        defaults to False
    :type normalize: bool, optional
    :return: The entropy of a series
    :rtype: float
    """

    def expectation(probability: Series) -> float:
        """Compute the expectation of a serie."""
        return (probability * np.log(probability) / np.log(base)).sum()

    def efficiency(entropy: float, length: int) -> float:
        """compute the efficiency of a serie."""
        return entropy * np.log(base) / np.log(length)

    probability = series.value_counts(normalize=True, sort=False)
    h = -expectation(probability)
    _entropy = efficiency(h, series.size) if normalize else h
    return _entropy


# compute the entropy of a dataframe
def get_entropies(
    dataframe: DataFrame,
    *,
    base: int = 2,
    normalize: bool = False,
) -> Series:
    """Compute the entropies of each attribute of a dataset.

    By default it computes the Shannon entropy (base two).

    :param dataframe: a dataset with named attributes
    :type dataframe: DataFrame
    :param base: log base of the entropy, defaults to 2
    :type base: int, optional
    :param normalize:  when True the entropy is nomalized by computing
        the efficiency of the expectation fo the sequence,
        defaults to False
    :type normalize: bool, optional
    :return: The entropies of each attribute of a dataset
    :rtype: Series
    """
    dataframe_ = dataframe.copy()
    entropies = dataframe_.apply(entropy, base=base, normalize=normalize)
    return (
        entropies.to_frame()
        .reset_index()
        .rename(
            {
                "index": "attribute",
                0: "entropy",
            },
            axis=1,
        )
    )
